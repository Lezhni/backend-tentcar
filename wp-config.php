<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'tentcar');

/** Имя пользователя MySQL */
define('DB_USER', 'tentcar');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '3C5i5F1f');

/** Имя сервера MySQL */
define('DB_HOST', '95.47.99.17');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tVnIb:[E&#`sC+}xyT=`4a??-^l7;G?!wV:ufX#a!B-?0FR^Zb$`0zA,F|PnpG9d');
define('SECURE_AUTH_KEY',  'oHOFVPvclZH+_%Rp6)Ij&5i1Lm2n9w,;/iBR[21LOt|s.rbSM*z#IXJ{9sOA<LMh');
define('LOGGED_IN_KEY',    '@7APUCkvtu>A|D:#fM8oR>l^lCc]3=bK;j87/pkD5k&p}jCrdNldzqOvD@{ekns<');
define('NONCE_KEY',        'kxg4Ezq7LCdb0fB-hG:>1~^:;,Nf&.y;#G5zW?Gp*+R6/+c-9zw 3Ur;{;Lxucfr');
define('AUTH_SALT',        '=B{]Kg1GrN^S,U<pc><pS5779$+4+#(Bamb;L}9B?O*jK2PsIk/s%9W*5z~On+_G');
define('SECURE_AUTH_SALT', '4XGNcCF5UvRy`lRjM%naR)jNI3/y;/~-Js.;NDjD1.(d2B74S?ygv6mTgQd8EVT.');
define('LOGGED_IN_SALT',   'ChB-@jDdIoZIy3{c?aFfc+vtp%<LC(LD%:$Gb P6/i;PFud!3{9[>f@@1]n2|pk?');
define('NONCE_SALT',       'X~gvUGU!LBAYrIOGwhR5yp +>gVrMNKy%sJ5Q^!Unr!&c0QQ`SXi62[ix-]2^9=7');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WP_HOME','http://tentcar.dev');
define('WP_SITEURL','http://tentcar.dev');

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
