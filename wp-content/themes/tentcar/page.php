<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<section class="text-page-content">
    <div class="container">
        <div class="text-page-img">
            <div class="text-page-title">
                <h1 class="block-title"><?php the_title(); ?></h1>
            </div>
        </div>
        <div class="container-inner inner-page-content">
            <?php the_content(); ?>
        </div>
    </div>
</section>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
