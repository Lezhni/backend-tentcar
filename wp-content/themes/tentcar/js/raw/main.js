$ = jQuery;
var sliderFirstChange = false;

$(document).on('ready', ready);
$(document).on('scroll', scrolling);
$(window).on('resize', resizing);
$(window).on('load', loaded);

function ready() {

    var url = document.location.href;
    $('.header-menu a').each(function() {

        var href = $(this).attr('href');
        if (((href.indexOf('catalog') > -1) && (url.indexOf('catalog') > -1)) || ((href.indexOf('portfolio') > -1) && (url.indexOf('portfolio') > -1))) {
            $(this).closest('li').addClass('current-page-ancestor');
        }
    });

    if ($('.header-menu .current-menu-item').length || $('.header-menu .current-page-ancestor').length) {

        var left = $('.header-menu .current-menu-item, .header-menu .current-page-ancestor').position().left;
        var width = $('.header-menu .current-menu-item, .header-menu .current-page-ancestor').innerWidth();

        $('.header-menu-line').css({
            left: left,
            width: width
        });
    }

    $('input[name="phone"]').inputmask({
  	    mask: '+7 (999) 999-99-99',
  	    showMaskOnHover: false,
  	    showMaskOnFocus: true,
  	});

    if ($(window).innerWidth() <= 768) {
        var isVertical = false;
        var isVerticalScrolling = false;
    } else {
        var isVertical = true;
        var isVerticalScrolling = true;
    }

    $('#slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        vertical: isVertical,
        verticalSwiping : isVerticalScrolling,
        arrows: false,
        dots: true
    });

    var slidesMaxHeight = 0;
    $('.slick-vertical .slick-slide:not(.slick-clone)').each(function() {

        if ($(this).height() > slidesMaxHeight) {
            slidesMaxHeight = $(this).height();
        }
    });
    $('.slick-vertical .slick-slide').height(slidesMaxHeight);

    $('.inner-page-content table a').attr('target', '_blank');

    $('.portfolio-category-list, .index-portfolio-list').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image %curr%',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1]
		}
	});

    $('.attached-portfolio-list .foogallery-container').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image %curr%',
		mainClass: 'mfp-img-mobile mfp-gallery',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1]
		},
        callbacks: {
            beforeOpen: function() {
                $('body').addClass('mfp-gallery');
            },
            close: function() {
                $('body').removeClass('mfp-gallery');
            }
        }
	});
}

var map, placemark;
ymaps.ready(function(){
    map = new ymaps.Map("tentcar_map", {
        center: [55.660242, 37.72891],
        zoom: 13
    });

    map.controls
        .add('zoomControl')
        .add('typeSelector')
        .add('mapTools');

    map.behaviors.disable(['scrollZoom']);

    placemark = new ymaps.Placemark([55.660242, 37.72891], {}, {
        preset: 'twirl#yellowIcon'
    });
    map.geoObjects.add(placemark);
});

function scrolling() {

    if ($('#slider').visible() && !sliderFirstChange) {
        
        setTimeout(function() {
            $('#slider').slick('slickNext');
        }, 1000);
        sliderFirstChange = true;
    }
}

function resizing() {}

function loaded() {

    $('.animate').on('scrollSpy:enter', function() {

       if (!$(this).hasClass('visible')) {
           $(this).addClass('visible');
       }
    });
    $('.animate').scrollSpy();
}

$('.header-menu li').hover(function() {

    var left = $(this).position().left;
    var width = $(this).innerWidth();

    $('.header-menu-line').css({
        left: left,
        width: width
    });
}, function() {

    if ($('.header-menu .current-menu-item').length || $('.header-menu .current-page-ancestor').length) {

        var left = $('.header-menu .current-menu-item, .header-menu .current-page-ancestor').position().left;
        var width = $('.header-menu .current-menu-item, .header-menu .current-page-ancestor').innerWidth();

        $('.header-menu-line').css({
            left: left,
            width: width
        });
    }
});


$('.item-desc-more').click(function() {

    $('.item-desc').toggleClass('full-content');
    return false;
});

$('.callback').click(function() {

    var target = $(this).attr('href');

    $.magnificPopup.open({
        items: {
            src: target
        },
        type: 'inline'
    });
    return false;
});

$('.mobile-menu').click(function() {

    $('.mobile-menu, .header-menu').toggleClass('opened');
});

$('.item-gallery a').click(function() {

    var img = $(this).attr('href');
    $('.item-img a[data-lightbox] img').removeAttr('srcset');
    $('.item-img a[data-lightbox]').attr('href', img);
    $('.item-img a[data-lightbox] img').attr('src', img);
    return false;
});

$(document).on('click', '.tab-nav a:not(.tab-active)', function() {

    var targetTab = $(this).attr('href');
    $('.tab-nav a, .tab').removeClass('tab-active');
    $(this).addClass('tab-active');
    $(targetTab).addClass('tab-active');

    $('[name="selected-tab"]').val(targetTab);

    return false;
});
