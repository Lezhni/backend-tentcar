    <footer role="contentinfo">
        <div class="container">
            <div class="header-phone footer-phone">
                <?php $formated_phone = preg_replace( '/[^0-9.\+]+/', '', get_option( 'phone' ) ); ?>
                <a href="tel:<?php echo $formated_phone; ?>" class="header-phone-tel"><?php echo get_option( 'phone' ); ?></a>
                <a href="#callback" class="header-phone-callback callback">заказать звонок</a>
            </div>
            <small class="footer-info">ТентКар.ру - все права защищены</small>
            <a href="http://artpages.pro" class="footer-developer" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/artpages-logo.png" alt=""></a>
        </div>
    </footer>
    <?php wp_footer(); ?>
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="//api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
</body>
</html>
