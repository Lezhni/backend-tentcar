<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="single-item-content">
    <div class="item-info to-left">
        <nav class="breadcrumbs"><ul><?php if (function_exists( 'bcn_display' ) ) bcn_display(); ?></ul></nav>
        <h2 class="item-title"><?php the_title(); ?></h2>
    </div>
    <div class="clearfix"></div>
</div>

<?php $attachedGallery = get_field( 'attached-gallery' ); if ( !empty( $attachedGallery ) ) : ?>
    <?php $galleryShortcode = "[foogallery id='{$attachedGallery[0]->ID}']"; ?>
    <div class="attached-portfolio-list">
    	<?php echo do_shortcode( htmlspecialchars_decode($galleryShortcode) ); ?>
    </div>
<?php endif; ?>

<?php endwhile; endif; ?>
