<?php

// add style.min.css to head
function enqueue_styles() {
    wp_register_style( 'main-style', get_template_directory_uri() . '/css/style.min.css', array(), '1', 'all' );
    wp_enqueue_style( 'main-style' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles' );

// add navigation area
function register_menu() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Верхнее меню' ),
            'catalog-menu' => __( 'Меню каталога' ),
            'portfolio-menu' => __( 'Меню портфолио' )
        )
    );
}
add_action( 'init', 'register_menu' );

// disable `posts`
function remove_posts_menu() {
    remove_menu_page( 'edit.php' );
    remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_init', 'remove_posts_menu' );

// add `catalog` post type
function create_posts_type() {
	register_post_type( 'catalog',
		array(
		    'labels' => array(
		  	    'name' => __( 'Каталог' ),
			    'singular_name' => __( 'Каталог' )
			),
		    'public' => true,
		    'has_archive' => true,
		    'show_in_nav_menus' => true,
		    'hierarchical' => true,
		    'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
		)
	);

    register_post_type( 'portfolio',
		array(
		    'labels' => array(
		  	    'name' => __( 'Фото и видео' ),
			    'singular_name' => __( 'Фото и видео' )
			),
		    'public' => true,
            'rewrite' => array(
                'slug' => 'foto-i-video'
            ),
		    'has_archive' => true,
		    'show_in_nav_menus' => true,
		    'hierarchical' => true,
		    'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
		)
	);
}
add_action( 'init', 'create_posts_type' );

// enable post thumbnails
add_theme_support( 'post-thumbnails' );

// function that get post info by title
function get_post_by_title( $page_title, $post_type ='post', $output = OBJECT ) {
    global $wpdb;
    $post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s", $page_title, $post_type ) );
    if ( $post ) return get_post( $post, $output );
    return null;
}

// function that get post by slug (for custom posts)
function get_post_id( $slug, $post_type ) {
    $query = new WP_Query( array( 'name' => $slug, 'post_type' => $post_type ) );
    $query->the_post();
    return get_the_ID();
}

// function for remove html from excerpt
function custom_wp_trim_excerpt( $text ) {
    $raw_excerpt = $text;
    if ( '' == $text ) {
        $text = get_the_content( '' );
        $text = strip_shortcodes( $text );

        $text = apply_filters( 'the_content', $text );
        $text = str_replace( ']]>', ']]&gt;', $text );

        $allowed_tags = '';
        $text = strip_tags( $text, $allowed_tags );

        $excerpt_word_count = 55;
        $excerpt_length = apply_filters( 'excerpt_length', $excerpt_word_count );

        $excerpt_end = '..';
        $excerpt_more = apply_filters( 'excerpt_more', ' ' . $excerpt_end );

        $words = preg_split( "/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY );
        if ( count( $words ) > $excerpt_length ) {
            array_pop( $words );
            $text = implode(' ', $words );
            $text = $text . $excerpt_more;
        } else {
            $text = implode( ' ', $words );
        }
    }
    return apply_filters( 'wp_trim_excerpt', $text, $raw_excerpt );
}
remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );
add_filter( 'get_the_excerpt', 'custom_wp_trim_excerpt' );
