<?php get_header(); ?>

<section class="promo">
    <div class="dark-overlay"></div>
    <div class="container">
        <div class="promo-content to-left animate">
            <?php
                $indexPage = get_page_by_path( 'index' );
                $indexPageID = $indexPage->ID;
            ?>
            <?php echo $indexPage->post_content; ?>
        </div>
        <div class="promo-form to-right animate">
            <?php echo do_shortcode( '[contact-form-7 id="37" title="Обратный звонок" html_class="to-right"]' ); ?>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>
<section class="index-double">
    <div class="container">
        <div class="index-double-about to-left">
            <h2 class="block-title">О компании</h2>
            <?php echo get_field( 'index-about', $indexPageID ); ?>
        </div>
        <div class="index-double-advantages to-right">
            <h2 class="block-title">Преимущества</h2>
            <div class="advantages-list">
                <span class="advantages-line"></span>
                <?php echo get_field( 'index-advantages', $indexPageID ); ?>
                <div class="clearfix mobile-clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>
<section class="index-slider">
    <div class="bg-overlay"></div>
    <div class="slider" id="slider">
        <?php query_posts(
            array(
                'numberposts' => 10,
            	'post_type' => 'catalog',
            	'meta_key' => 'catalog-inslider',
            	'meta_value' => true
            )
        ); ?>

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="slide">
            <div class="container">
                <figure class="slide-img">
                    <?php the_post_thumbnail( array( 380 ) ); ?>
                </figure>
                <article class="slide-content">
                    <h3 class="slide-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <?php the_excerpt(); ?>
                </article>
            </div>
        </div>
        <?php endwhile; endif; wp_reset_query(); ?>

    </div>
</section>
<section class="index-portfolio">
    <div class="container">
        <h2 class="block-title">Портфолио</h2>
        <article class="index-portfolio-content">
            <?php echo get_field( 'index-portfolio', $indexPageID ); ?>
        </article>
    </div>
    <div class="index-portfolio-list">
        <?php query_posts(
            array(
                'numberposts' => 10,
            	'post_type' => 'page',
            	'meta_key' => 'portfolio-onindex',
            	'meta_value' => true
            )
        ); ?>

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <a href="<?php the_post_thumbnail_url(); ?>" class="portfolio-single">
            <div class="portfolio-single-inner">
                <div class="portfolio-hover">
                    <span class="top-decor"></span>
                    <span class="right-decor"></span>
                    <div class="hover-content">
                        <i class="fa fa-search"></i>
                        <h3 class="portfolio-single-title"><?php the_title(); ?></h3>
                    </div>
                    <span class="bottom-decor"></span>
                    <span class="left-decor"></span>
                </div>
                <?php the_post_thumbnail(); ?>
            </div>
        </a>
        <?php endwhile; endif; wp_reset_query(); ?>

        <div class="clearfix"></div>
    </div>
</section>
<section class="index-contacts">
    <div class="container">
        <h2 class="block-title">Наши контакты</h2>
        <?php
            $contactsPage = get_page_by_path( 'contacts' );
            $contactsPageID = $contactsPage->ID;
        ?>
        <div class="contacts-phones to-left animate">
            <strong><i class="fa fa-mobile"></i>Телефоны:</strong>
            <?php echo get_field( 'contacts-phones', $contactsPageID ); ?>
        </div>
        <div class="contacts-shedule to-left animate">
            <strong><i class="fa fa-clock-o"></i>Режим работы:</strong>
            <?php echo get_field( 'contacts-shedule', $contactsPageID ); ?>
        </div>
        <div class="contacts-email to-left">
            <strong><i class="fa fa-at"></i>E-mail:</strong>
            <p><a href="mailto:<?php echo get_option( 'email' ); ?>"><?php echo get_option( 'email' ); ?></a></p>
        </div>
        <div class="clearfix"></div>
        <p class="contacts-map animate"><strong><i class="fa fa-map-marker"></i>Адрес:</strong> <?php echo get_option( 'address' ); ?></p>
        <div class="promo-form index-contacts-form to-right animate">
            <?php echo do_shortcode( '[contact-form-7 id="36" title="Обратный звонок" html_class="to-right"]' ); ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <div id="tentcar_map"></div>
</section>
<div class="clearfix"></div>

<?php get_footer(); ?>
