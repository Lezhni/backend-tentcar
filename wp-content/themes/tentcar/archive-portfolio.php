<?php get_header(); ?>

<section class="catalog-page-content photovideo-page-content">
    <div class="container">
        <h3 class="block-title">Фото и видео</h3>
        <div class="catalog-content parent-catalog-content to-right">

            <div class="item-list parent-item-list">

                <?php $pagesCat = new WP_Query(
                    array(
                        'showposts' => -1,
                        'post_type' => 'portfolio',
                        'orderby' => 'menu_order',
                        'order' => 'DESC'
                    )
                ); ?>

                <?php $num = 0; ?>
                <?php if ( $pagesCat->have_posts() ) : $catNum = 0; while( $pagesCat->have_posts() ) : $pagesCat->the_post(); ?>

                    <?php if ( $num == 0 ) : ?>
                        <div class="clearfix"></div>
                        <h2 class="catalog-content-category-title">Шторы и завесы</h2>
                        <div class="clearfix"></div>
                    <?php endif; ?>

                    <?php if ( $num == 5 ) : ?>
                        <div class="clearfix"></div>
                        <h2 class="catalog-content-category-title">Тентовые конструкции</h2>
                        <div class="clearfix"></div>
                    <?php endif; ?>

                    <?php if ( $num == 20 ) : ?>
                        <div class="clearfix"></div>
                        <h2 class="catalog-content-category-title">Изделия для автотранспорта</h2>
                        <div class="clearfix"></div>
                    <?php endif; ?>

                    <?php if ( $num == 28 ) : ?>
                        <div class="clearfix"></div>
                        <h2 class="catalog-content-category-title">Пологи, тенты для техники и стройки</h2>
                        <div class="clearfix"></div>
                    <?php endif; ?>

                    <?php if ( $num == 32 ) : ?>
                        <div class="clearfix"></div>
                        <h2 class="catalog-content-category-title">Спортивные маты, покрышки и бассейны</h2>
                        <div class="clearfix"></div>
                    <?php endif; ?>

                    <?php if ( $num == 37 ) : ?>
                        <div class="clearfix"></div>
                        <h2 class="catalog-content-category-title">Услуги</h2>
                        <div class="clearfix"></div>
                    <?php endif; ?>

                    <a href="<?php the_permalink(); ?>" class="item-single">
                        <div class="item-single-inner">
                            <div class="item-single-overlay"></div>
                            <?php if ( has_post_thumbnail() ) : ?>
                                <?php the_post_thumbnail(); ?>
                            <?php else : ?>
                                <img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=Нет+фото&w=300&h=220">
                            <?php endif; ?>
                            <p class="item-single-title">
                                <?php the_title(); ?>
                            </p>
                        </div>
                    </a>
                <?php $num++; ?>
                <?php endwhile; endif; wp_reset_query(); ?>

            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>

<?php get_footer(); ?>
