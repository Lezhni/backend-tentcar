<?php get_header(); ?>

<section class="catalog-page-content">
    <div class="container">
        <aside class="catalog-sidebar to-left">
            <h3 class="block-title">Каталог</h3>
            <nav class="catalog-menu catalog-main-menu">
                <?php wp_nav_menu( array( 'theme_location' => 'catalog-menu' ) ); ?>
            </nav>
        </aside>
        <div class="catalog-content parent-catalog-content to-right">

            <div class="item-list parent-item-list">

                <?php
                    $pagesCat = new WP_Query(
                    array(
                        'showposts' => -1,
                        'post_parent' => 0,
                        'post_type' => 'catalog',
                        'orderby' => 'menu_order',
                        'order' => 'DESC'
                    )
                ); ?>

                <?php if ( $pagesCat->have_posts() ) : while( $pagesCat->have_posts() ) : $pagesCat->the_post(); ?>
                    <div class="clearfix"></div>
                    <h2 class="catalog-content-category-title"><?php the_title(); ?></h2>

                    <?php
                        $pagesChild = new WP_Query(
                        array(
                            'showposts' => -1,
                            'post_parent' => get_the_ID(),
                            'post_type' => 'catalog',
                            'orderby' => 'menu_order',
                            'order' => 'DESC'
                        )
                    ); ?>

                    <?php if ( $pagesChild->have_posts() ) : while( $pagesChild->have_posts() ) : $pagesChild->the_post(); ?>
                        <a href="<?php the_permalink(); ?>" <?php post_class( 'item-single' ); ?>>
                            <div class="item-single-inner">
                                <div class="item-single-overlay"></div>
                                <?php if ( has_post_thumbnail() ) : ?>
                                    <?php the_post_thumbnail( array(220, 220) ); ?>
                                <?php else : ?>
                                    <img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=Нет+фото&w=300&h=220">
                                <?php endif; ?>
                                <p class="item-single-title">
                                    <?php the_title(); ?>
                                    <span class="item-single-desc"><?php echo get_the_excerpt(); ?></span>
                                </p>
                            </div>
                        </a>

                        <?php
                            $pagesChildChild = new WP_Query(
                            array(
                                'showposts' => -1,
                                'post_parent' => get_the_ID(),
                                'post_type' => 'catalog',
                                'orderby' => 'menu_order',
                                'order' => 'DESC'
                            )
                        ); ?>

                        <?php if ( $pagesChildChild->have_posts() ) : while( $pagesChildChild->have_posts() ) : $pagesChildChild->the_post(); ?>
                            <a href="<?php the_permalink(); ?>" <?php post_class( 'item-single' ); ?>>
                                <div class="item-single-inner">
                                    <div class="item-single-overlay"></div>
                                    <?php if ( has_post_thumbnail() ) : ?>
                                        <?php the_post_thumbnail( array(220, 220) ); ?>
                                    <?php else : ?>
                                        <img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=Нет+фото&w=300&h=220">
                                    <?php endif; ?>
                                    <p class="item-single-title">
                                        <?php the_title(); ?>
                                        <span class="item-single-desc"><?php echo get_the_excerpt(); ?></span>
                                    </p>
                                </div>
                            </a>

                            <?php
                                $pagesChildChildChild = new WP_Query(
                                array(
                                    'showposts' => -1,
                                    'post_parent' => get_the_ID(),
                                    'post_type' => 'catalog',
                                    'orderby' => 'menu_order',
                                    'order' => 'ASC'
                                )
                            ); ?>

                            <?php if ( $pagesChildChildChild->have_posts() ) : while( $pagesChildChildChild->have_posts() ) : $pagesChildChildChild->the_post(); ?>
                                <a href="<?php the_permalink(); ?>" <?php post_class( 'item-single' ); ?>>
                                    <div class="item-single-inner">
                                        <div class="item-single-overlay"></div>
                                        <?php if ( has_post_thumbnail() ) : ?>
                                            <?php the_post_thumbnail( array(220, 220) ); ?>
                                        <?php else : ?>
                                            <img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=Нет+фото&w=300&h=220">
                                        <?php endif; ?>
                                        <p class="item-single-title">
                                            <?php the_title(); ?>
                                            <span class="item-single-desc"><?php echo get_the_excerpt(); ?></span>
                                        </p>
                                    </div>
                                </a>
                            <?php endwhile; endif; wp_reset_query(); ?>

                        <?php endwhile; endif; wp_reset_query(); ?>

                    <?php endwhile; endif; wp_reset_query(); ?>

                <?php endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>

<?php get_footer(); ?>
