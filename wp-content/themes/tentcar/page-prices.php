<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<section class="text-page-content">
    <div class="container">
        <div class="text-page-img">
            <div class="text-page-title">
                <h1 class="block-title"><?php the_title(); ?></h1>
            </div>
        </div>
        <div class="container-inner inner-page-content">
            <?php the_content(); ?>

            <?php $itemPriceLists = get_field( 'price-list-items' ); if ( count( $itemPriceLists ) > 0 ) : ?>
                <div class="prices-list">

                    <?php foreach ( $itemPriceLists as $singlePrice ) : if ( !$singlePrice['price-list-item-file'] ) continue; ?>
                        <a href="<?= $singlePrice['price-list-item-file']; ?>" target="__blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/price-icon.png" width="70" /><br>
                            <strong><?= $singlePrice['price-list-item-title']; ?></strong>
                        </a>
                    <?php endforeach; ?>

                </div>
            <?php endif; ?>

        </div>
    </div>
</section>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
