<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="item-content">
    <div class="item-info to-left">
        <nav class="breadcrumbs"><ul><?php if (function_exists( 'bcn_display' ) ) bcn_display(); ?></ul></nav>
        <h2 class="item-title"><?php the_title(); ?></h2>
        <article class="item-desc">
            <figure class="item-img to-right">
                <?php the_post_thumbnail(); ?>
            </figure>
            <?php the_content(); ?>
        </article>
    </div>
    <div class="clearfix"></div>
</div>

<div class="item-list parent-item-list">

    <?php $pagesCat = new WP_Query(
        array(
            'posts_per_page' => -1,
            'post_parent' => get_the_ID(),
            'post_type' => 'catalog',
            'depth' => 1,
            'order' => 'ASC'
        )
    ); ?>

    <?php if ( $pagesCat->have_posts() ) : while( $pagesCat->have_posts() ) : $pagesCat->the_post(); ?>
    <?php if ( !wp_get_post_parent_id( get_the_ID() ) ) continue; ?>
    <a href="<?php the_permalink(); ?>" class="item-single">
        <div class="item-single-inner">
            <div class="item-single-overlay"></div>
            <?php if ( has_post_thumbnail() ) : ?>
                <?php the_post_thumbnail(); ?>
            <?php else : ?>
                <img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=Нет+фото&w=300&h=220">
            <?php endif; ?>
            <p class="item-single-title">
                <?php the_title(); ?>
                <span class="item-single-desc"><?php echo get_the_excerpt(); ?></span>
            </p>
        </div>
    </a>
    <?php endwhile; endif; wp_reset_query(); ?>

</div>
<?php endwhile; endif; ?>
