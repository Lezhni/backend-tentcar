<!doctype html>
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> prefix="http://ogp.me/ns#"> <!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="dns-prefetch" content="//fonts.googleapis.com">
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#222">
    <!--[if lt IE 9]>
  	<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<![endif]-->
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:100&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Caption:700,400&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    <link rel="shortcut icon" href="/favicon.ico">
    <title><?php wp_title(); ?></title>

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
    <div class="mfp-hide popup" id="callback">
        <?php echo do_shortcode( '[contact-form-7 id="24" title="Обратный звонок"]' ); ?>
    </div>
    <div class="mfp-hide popup" id="order">
        <?php echo do_shortcode( '[contact-form-7 id="24" title="Обратный звонок"]' ); ?>
    </div>
    <address class="top-panel">
        <div class="container">
            <a href="mailto:<?php echo get_option( 'email' ); ?>" class="top-panel-email to-left"><i class="fa fa-envelope"></i><?php echo get_option( 'email' ); ?></a>
            <?php $formated_phone = preg_replace( '/[^0-9.\+]+/', '', get_option( 'phone' ) ); ?>
            <a href="tel:<?php echo $formated_phone; ?>" class="top-panel-phone to-left"><i class="fa fa-phone"></i><?php echo get_option( 'phone' ); ?></a>
            <p class="top-panel-map to-left"><i class="fa fa-map-marker"></i><?php echo get_option( 'address' ); ?></p>
            <p class="top-panel-shedule to-right"><i class="fa fa-clock-o"></i><?php echo get_option( 'shedule' ); ?></p>
            <div class="clearfix"></div>
        </div>
    </address>
    <header role="banner">
        <div class="container">
            <a href="<?php echo site_url(); ?>" class="header-logo to-left"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="TentCar"></a>
            <span class="mobile-menu to-right"><svg enable-background="new 0 0 24 24" id="Layer_1" version="1.0" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><circle cx="12" cy="12" r="2"/><circle cx="12" cy="5" r="2"/><circle cx="12" cy="19" r="2"/></svg></span>
            <div class="clearfix mobile-clearfix"></div>
            <nav class="header-menu to-left">
                <?php wp_nav_menu( array( 'menu' => 'header-menu' ) ); ?>
                <span class="header-menu-line"></span>
            </nav>
            <div class="header-phone to-right">
                <a href="tel:<?php echo $formated_phone; ?>" class="header-phone-tel"><?php echo get_option( 'phone' ); ?></a>
                <a href="#callback" class="header-phone-callback callback">заказать звонок</a>
            </div>
        </div>
    </header>
