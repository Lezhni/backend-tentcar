<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="single-item-content">
    <div class="item-info to-left">
        <nav class="breadcrumbs"><ul><?php if (function_exists( 'bcn_display' ) ) bcn_display(); ?></ul></nav>
        <h2 class="item-title"><?php the_title(); ?></h2>
        <article class="item-desc">
            <figure class="item-img to-right">
                <a href="<?php the_post_thumbnail_url(); ?>" data-lightbox="item"><?php the_post_thumbnail(); ?></a>
                <div class="item-order">
                    <a href="#order" class="item-order-button callback">заказать</a>
                </div>

                <?php $itemPriceLists = get_field( 'catalog-price' ); if ( count( $itemPriceLists ) > 0 ) : ?>
                <div class="item-price-list to-right">

                    <?php foreach ( $itemPriceLists as $singlePrice ) : if ( !$singlePrice['catalog-price-single'] ) continue; ?>
                    <a href="<?= $singlePrice['catalog-price-single']; ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/price-icon.png" width="70" />
                        <span class="item-price-list-title"><?= $singlePrice['catalog-price-single-name']; ?></span>
                    </a>
                    <?php endforeach; ?>

                    <div class="clearfix"></div>
                </div>
                <div class="prices-list-link">
                    <a href="/prices">Другие прайс-листы</a>
                </div>
                <?php endif; ?>

            </figure>
            <?php echo the_content(); ?>
        </article>
    </div>
    <div class="clearfix"></div>
</div>

<?php $attachedGallery = get_field( 'attached-gallery-shortcode' ); if ( !empty( $attachedGallery ) ) : ?>
    <?php
        $galleryShortcode = "[foogallery id='{$attachedGallery[0]->ID}']";
        $currentSlug = get_post_field( 'post_name', get_post() );
    ?>
    <div class="attached-portfolio-list catalog-attached-portfolio-list">
        <h4 class="item-list-title">Фото наших работ</h4>
    	<?php echo do_shortcode( htmlspecialchars_decode($galleryShortcode) ); ?>
        <div class="attached-portfolio-list-link"><a href="/foto-i-video/<?= $currentSlug; ?>">Все фото этого раздела</a></div>
    </div>
<?php endif; ?>

<div class="item-list">
    <?php $pagesCat = new WP_Query(
        array(
            'posts_per_page' => -1,
            'post_parent' => wp_get_post_parent_id( get_the_ID() ),
            'post__not_in' => array( get_the_ID() ),
            'post_type' => 'catalog',
            'depth' => 1,
            'order' => 'ASC'
        )
    ); ?>

    <?php if ( $pagesCat->have_posts() ) : ?>
    <h4 class="item-list-title">Другая продукция из данной категории</h4>

        <?php while( $pagesCat->have_posts() ) : $pagesCat->the_post(); ?>
        <a href="<?php the_permalink(); ?>" class="item-single">
            <div class="item-single-inner">
                <div class="item-single-overlay"></div>
                <?php if ( has_post_thumbnail() ) : ?>
                    <?php the_post_thumbnail( array(220, 220) ); ?>
                <?php else : ?>
                    <img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=Нет+фото&w=300&h=220">
                <?php endif; ?>
                <p class="item-single-title">
                    <?php the_title(); ?>
                    <span class="item-single-desc"><?php echo get_the_excerpt(); ?></span>
                </p>
            </div>
        </a>
        <?php endwhile; ?>

     <?php endif; wp_reset_query(); ?>
</div>
<?php endwhile; endif; ?>
