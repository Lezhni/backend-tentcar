<?php /* Template Name: Контакты */ ?>

<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<section class="index-contacts contacts-page-content">
    <div class="container">
        <h2 class="block-title"><?php the_title(); ?></h2>

        <?php if ( get_the_content() ) : ?>
            <article class="contacts-page-text">
                <?php the_content(); ?>
            </article>
        <?php endif; ?>

        <div class="contacts-phones to-left">
            <strong><i class="fa fa-mobile"></i>Телефоны:</strong>
            <?php echo get_field( 'contacts-phones' ); ?>
        </div>
        <div class="contacts-shedule to-left">
            <strong><i class="fa fa-clock-o"></i>Режим работы:</strong>
            <p><?php echo get_field( 'contacts-shedule' ); ?></p>
        </div>
        <div class="contacts-email to-left">
            <strong><i class="fa fa-at"></i>E-mail:</strong>
            <p><a href="mailto:<?php echo get_option( 'email' ); ?>"><?php echo get_option( 'email' ); ?></a></p>
        </div>
        <div class="clearfix"></div>
        <p class="contacts-map"><strong><i class="fa fa-map-marker"></i>Адрес:</strong> <?php echo get_option( 'address' ); ?></p>
        <div class="promo-form index-contacts-form to-right">
            <?php echo do_shortcode( '[contact-form-7 id="36" title="Обратный звонок" html_class="to-right"]' ); ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <div id="tentcar_map"></div>
</section>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
