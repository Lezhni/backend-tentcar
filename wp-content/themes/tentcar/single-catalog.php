<?php get_header(); ?>

<section class="catalog-page-content">
    <div class="container">
        <aside class="catalog-sidebar to-left">
            <h3 class="block-title">Каталог</h3>
            <nav class="catalog-menu">
                <?php wp_nav_menu( array( 'theme_location' => 'catalog-menu' ) ); ?>
            </nav>
        </aside>
        <div class="catalog-content to-right">
            <?php
                $children = get_pages( array( 'parent' => $post->ID, 'post_type' => 'catalog' ) );

                if( count( $children ) != 0 ) {
                    get_template_part( 'catalog', 'category' );
                } else {
                    get_template_part( 'catalog', 'item' );
                }
            ?>
        </div>
        <div class="clearfix"></div>
    </div>
</section>

<?php get_footer(); ?>
